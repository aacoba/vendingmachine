# Vendingmachine
## Auke Bakker
### PO H4 P4

## Gebruik
Download voor de .jar is hier te vinden
https://bitbucket.org/aacoba/vendingmachine/downloads
Grotendeels self explaiatory...
Gebruik itemID 1337 om af te sluiten


### Javadocs
De javadocs zijn te vinden in het mapje JDocs

### Licentie
Vendingmachine van Auke Bakker is in licentie gegeven volgens een Creative Commons Naamsvermelding-NietCommercieel-GeenAfgeleideWerken 3.0 Nederland licentie.
Ga naar http://creativecommons.org/licenses/by-nc-nd/3.0/nl/ om een kopie van de licentie te kunnen lezen.