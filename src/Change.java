/**
 * Created with IntelliJ IDEA.
 * Project: Vendingmachine
 * User: Aacoba+Vendingmachine@Aacoba
 * Date: 5/8/13
 * Time: 9:34 PM
 */
public class Change {

    public static String calcC(double cost, double paid) {
        // Calculate the change that has to be returned

        // Init some vars
        int hundred = 0;
        int fifty = 0;
        int twenty = 0;
        int ten = 0;
        int five = 0;
        int two = 0;
        int one = 0;
        int fiftyCt = 0;
        int twentyCt = 0;
        int tenCt = 0;
        int fiveCt = 0;

        cost = cost * 100; // Convert to cents, because integers > doubles
        paid = paid * 100; // Convert to cents, because integers > doubles
        int change = (int)paid - (int)cost;
        int Tchange = change;
        int leftOver;

        if (Tchange > 0) {
            //Calc Stuff
            if (change >= 10000){
                hundred = change / 10000;
                leftOver = hundred * 10000;
                change = change - leftOver;
            }
            if (change >= 5000) {
                fifty = change / 5000;
                leftOver = fifty * 5000;
                change = change - leftOver;
            }
            if (change >= 2000) {
                twenty = change / 2000;
                leftOver = twenty * 2000;
                change = change - leftOver;
            }
            if (change >= 1000) {
                ten = change / 1000;
                leftOver = ten * 1000;
                change = change - leftOver;
            }
            if (change >= 500) {
                five = change / 500;
                leftOver = five * 500;
                change = change - leftOver;
            }
            if (change >= 200) {
                two = change / 200;
                leftOver = two * 200;
                change = change - leftOver;
            }
            if (change >= 100) {
                one = change / 100;
                leftOver = one * 100;
                change = change - leftOver;
            }
            if (change >= 50) {
                fiftyCt = change / 50;
                leftOver = fiftyCt * 50;
                change = change - leftOver;
            }
            if (change >= 20) {
                twentyCt = change / 20;
                leftOver = twentyCt * 20;
                change = change - leftOver;
            }
            if (change >= 10) {
                tenCt = change / 10;
                leftOver = tenCt * 10;
                change = change - leftOver;
            }
            if (change >= 5) {
                fiveCt = change / 5;
                leftOver = fiveCt * 5;
                change = change - leftOver;
            }
            if (change >= 3) {
                fiveCt++;
            }
        }
        // Display the amount of change the user receives
        StringBuilder returnString = new StringBuilder();
        if (hundred > 0) { returnString.append("\n€100: ").append(hundred).append("x"); }
        if (fifty > 0) { returnString.append("\n€50: ").append(fifty).append("x"); }
        if (twenty > 0) { returnString.append("\n€20: ").append(twenty).append("x"); }
        if (ten > 0) { returnString.append("\n€10: ").append(ten).append("x"); }
        if (five > 0) { returnString.append("\n€5: ").append(five).append("x"); }
        if (two > 0) { returnString.append("\n€2: ").append(two).append("x"); }
        if (one > 0) { returnString.append("\n€1: ").append(one).append("x"); }
        if (fiftyCt > 0) { returnString.append("\n€0.50: ").append(fiftyCt).append("x"); }
        if (twentyCt > 0) { returnString.append("\n€0.20: ").append(twentyCt).append("x"); }
        if (tenCt > 0) { returnString.append("\n€0.10: ").append(tenCt).append("x"); }
        if (fiveCt > 0) { returnString.append("\n€0.05: ").append(fiveCt).append("x"); }
        return returnString.toString();


    }
}
