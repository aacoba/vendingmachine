import java.text.DecimalFormat;

import static java.lang.String.format;

/**
 * Created with IntelliJ IDEA.
 * Project: VendingMachine
 * User: Aacoba+VendingMachine@Aacoba.net
 * Date: 5/5/13
 * Time: 3:13 PM
 */
public class Item {
    private int id;
    private  String name;
    private double price;
    private int stock;

    public Item(int id, String name, double price, int stock) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
    }

    public int getID() {
        /* Return the ID */
        return id;
    }

    public String getName() {
        /** Return the name */
        return name;
    }

    public double getPrice() {
        /**
         * Return the price
        */
        return price;
    }

    public int getStock() {
        /**
         * Return the stock
         */
        return stock;
    }

    public boolean checkStock() {
        /**
         * Return if stock > 0
         */
        return stock > 0;
    }

    public void setStock(int newStock) {
        /**
         * Set the stock to
         * @param newStock New stock i guess....
         */
        stock = newStock;
    }

    public String fPrice() {
        /**
         * Format the price to €0.00
         */
        DecimalFormat df = new DecimalFormat("'€'0.00");
        return df.format(price);
    }

    public String tbl() {
        /**
         * Format the values to a fancy table :>
         */
        return format("%02d %-40s %-7s %2d%n", id, name, fPrice(), stock);
    }
}
