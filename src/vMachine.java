import javax.swing.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import static java.lang.String.format;

/**
 * Created with IntelliJ IDEA.
 * Project: VendingMachine
 * User: Aacoba+VendingMachine@Aacoba.net
 * Date: 5/5/13
 * Time: 3:12 PM
 */
public class vMachine {
    public static ArrayList<Item> itemList = new ArrayList<>();
    private static double revenue = 0;


    public static void main(String[] args) {
        defItems();
        //noinspection InfiniteLoopStatement
        while (true){
            Item choice = getItem(insertChoice());
            JOptionPane.showMessageDialog(null, "Uw keuze: " + choice.getName() + " voor " + choice.fPrice(), choice.getName(), JOptionPane.PLAIN_MESSAGE);
            String change;
            change = Change.calcC(choice.getPrice(), getMoney(choice));
            revenue = revenue + choice.getPrice();
            if (!change.isEmpty()) JOptionPane.showMessageDialog(null, "Uw wisselgeld is: " + change, "Wisselgeld", JOptionPane.PLAIN_MESSAGE);
            JOptionPane.showMessageDialog(null, "Neem uw aankoop uit\nEnjoy!", "Bedankt!", JOptionPane.PLAIN_MESSAGE);
            choice.setStock(choice.getStock() - 1); // Remove 1 from the stock
        }
    }

    public static void defItems() {
        /**
         * Define the items that are for sale (ID, Name, price, stock)
         */
        itemList.add(new Item(1, "Coca Cola", 0.30, randStock()));
        itemList.add(new Item(2, "Pepsi Cola", 0.30, randStock()));
        itemList.add(new Item(3, "Spa Rood", 0.30, randStock()));
        itemList.add(new Item(4, "Spa Blauw", 0.30, randStock()));
    }

    public static int randStock() {
        /**
         * Generate a random number between 3 and 20 for the stock...
         */
        int maxStock = 30;
        int minStock = 6;
        Random ran = new Random();
        return ran.nextInt(maxStock - minStock) + minStock;

    }

    public static String printItems() {
        int totalStockCount = 0;
        /**
         * Give the user a list of the items that are available
         */
        StringBuilder list = new StringBuilder(); // Create the string that contains the available items
        for (Item i : itemList) {
            totalStockCount = totalStockCount + i.getStock();
        }
        if (totalStockCount > 0) {
            list.append(format("%2s %-40s %-7s %8s%n", "ID", "Naam", "Prijs", "Vooraad"));
            for (Item i : itemList) { // For each in itemlist
                if (i.checkStock()) { // Only display if available
                    list.append(i.tbl());  //append to list
                }
            }

        } else {
            list.append("Geen Items meer op vooraad!");
        }
        return list.toString();  // return the list
    }

    public static int insertChoice() {
        /**
         * Get a valid int from the user to search for an item in the itemList
         */
        int id;
        try { // Get a valid int from the user
            id = Integer.parseInt(JOptionPane.showInputDialog(printItems(), "Voer uw keuze in"));
            if (id == 1337) {
                DecimalFormat df = new DecimalFormat("'€'0.00");
                JOptionPane.showMessageDialog(null, "Revenue this session: " + df.format(revenue));
                System.exit(0);
            }
            return id;
        } catch (NumberFormatException e) { // Catch if input is not a int
            JOptionPane.showMessageDialog(null, "Je knopjes drukken, niet op schroeven ofzo...", "Onjuiste input", JOptionPane.ERROR_MESSAGE); // Shout at the user
            return insertChoice(); // Call the method again
        }
    }

    public static Item getItem(int choice) {
        /**
         * Search the itemList for an item where the ID matches with choise
         * @param choice ID to search
         */
        boolean found = false;
        Item fItem = null;
        for (Item item : itemList) {
            if (item.getID() == choice) { // Check al the objects in the array for the ID
                fItem = item;
                if (!fItem.checkStock()) { // Check if the item is available..
                    JOptionPane.showMessageDialog(null, "Dit item is niet meer beschikbaar, maak een andere keuze", "Ontbrekende vooraad", JOptionPane.ERROR_MESSAGE);
                    return getItem(insertChoice()); // Ask the user again
                } else return item;
            }
        }
        /* @SuppressWarnings */
        if (!found) { // Display if no item is found
            JOptionPane.showMessageDialog(null, "Item ID is niet gevonden, maak een andere keuze", "Onbekende keuze", JOptionPane.ERROR_MESSAGE);
            return getItem(insertChoice());
        }
    return fItem;
    }

    public static double insertMoney(double restMoney) {
        /**
         * Get a valid double(Money) from the user
         */
        DecimalFormat df = new DecimalFormat("'€'0.00");
        double iMoney;
        try {
            iMoney = Double.parseDouble(JOptionPane.showInputDialog("Werp geld in...\nResterend: " + df.format(restMoney)));
            return iMoney;
        } catch (NumberFormatException e) { // Display if input isnt double
            JOptionPane.showMessageDialog(null, "Gooi geen knopen in de machine of de machine schiet een laser op je af!", "I IZ CHARGING MAH LASAR", JOptionPane.ERROR_MESSAGE);
            return insertMoney(restMoney);
        } catch (NullPointerException e) { // Display if input = null
            JOptionPane.showMessageDialog(null, "Gooi geen knopen in de machine of de machine schiet een laser op je af!", "I IZ CHARGING MAH LASAR", JOptionPane.ERROR_MESSAGE);
            return insertMoney(restMoney);
        }
    }

    public static double getMoney(Item item) {
        /**
         * Run @{@link insertMoney()} until there is enough money to buy the item
         * @param item The requested item object
         */
        double TIMoney = 0;
        while (TIMoney < item.getPrice()) TIMoney = TIMoney + insertMoney(item.getPrice() - TIMoney);
        return TIMoney;
    }
}
